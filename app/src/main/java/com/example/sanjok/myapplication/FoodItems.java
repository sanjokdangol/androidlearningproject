package com.example.sanjok.myapplication;


public class FoodItems {

    private String foodTitle;
    private String foodPrice;
    private String foodDesc;

    public void setFoodTitle(String foodTitle) {
        this.foodTitle = foodTitle;
    }

    public String getFoodPrice() {
        return foodPrice;
    }

    public void setFoodPrice(String foodPrice) {
        this.foodPrice = foodPrice;
    }

    public String getFoodDesc() {
        return foodDesc;
    }

    public void setFoodDesc(String foodDesc) {
        this.foodDesc = foodDesc;
    }

    public String getFoodTitle() {
        return foodTitle;
    }


}
