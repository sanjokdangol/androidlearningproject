package com.example.sanjok.myapplication;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;


public class FragmentHolderActivity extends AppCompatActivity implements FragmentInterface {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_holder);
    }

    @Override
    public void communicate(String data) {



        //getFragmentManager work for new FragmentManager
        //provide manager
        FragmentManager manager = getFragmentManager();

        Test2Fragment receiver = (Test2Fragment) manager.findFragmentById(R.id.frag_2);

        //show data to Test2Fragment
        receiver.display(data);

    }




}
