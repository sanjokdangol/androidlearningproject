package com.example.sanjok.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

/**
 * Created by sanjok on 9/13/17.
 */

public class GridviewActivity extends AppCompatActivity {

    GridView myGridView;

    FoodListActivity foodList;

    ArrayList<FoodItems> foodItems;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_gridview);

        myGridView = (GridView) findViewById(R.id.myGridView);

        foodList = new FoodListActivity();

        foodItems = foodList.setFoodItems();

        GridAdaptor adaptor = new GridAdaptor(getApplicationContext(),foodItems);

        myGridView.setAdapter(adaptor);

        myGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getApplicationContext(),DisplayItemActivity.class);
                i.putExtra("position",position);
                i.putExtra("food_name", foodItems.get(position).getFoodTitle());
                i.putExtra("food_price", foodItems.get(position).getFoodPrice());
                i.putExtra("food_desc", foodItems.get(position).getFoodDesc());

                startActivity(i);
            }
        });

    }




}
