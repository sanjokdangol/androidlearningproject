package com.example.sanjok.myapplication;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;


public class HoldFragmentsActivity extends AppCompatActivity {

    ViewPager pager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_holdfragments);

        pager = (ViewPager) findViewById(R.id.holder);

        FragmentManager myfm = getSupportFragmentManager();

        FragmentAdaptor adopter = new FragmentAdaptor(myfm);

        pager.setAdapter(adopter);


    }

    class FragmentAdaptor extends FragmentStatePagerAdapter{

        public FragmentAdaptor(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            Fragment fragment = new Fragment();


            switch(position){
                case 0:
                    fragment = new Fragment_a_Activity();
                    break;

                case 1:
                    fragment = new Fragment_b_Activity();
                    break;

                case 2:
                    fragment = new Fragment_c_Activity();
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            String fragment ="";


            switch(position){
                case 0:
                    fragment = "fragment a";
                    break;

                case 1:
                    fragment = "fragment b";
                    break;

                case 2:
                    fragment = "fragment c";
                    break;
            }
            return fragment;
        }
    }
}
