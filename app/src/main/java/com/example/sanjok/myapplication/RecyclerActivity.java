package com.example.sanjok.myapplication;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


public class RecyclerActivity extends AppCompatActivity {


    RecyclerView recyclerView;

    RecyclerView.LayoutManager manager;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_recycler);

        recyclerView = (RecyclerView) findViewById(R.id.recyler);

        FoodListActivity foodList = new FoodListActivity();

        //get food items datas from FoodListActivity
        ArrayList<FoodItems> items = foodList.setFoodItems();

        manager = new LinearLayoutManager(getApplicationContext());
        //manager = new GridLayoutManager(getApplicationContext(),3);

        //set manager to layout
        recyclerView.setLayoutManager(manager);

        RecyclerAdaptor adaptor = new RecyclerAdaptor(getApplicationContext(),items);
        recyclerView.setAdapter(adaptor);




    }
}
