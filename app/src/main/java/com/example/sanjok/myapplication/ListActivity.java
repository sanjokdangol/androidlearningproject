package com.example.sanjok.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class ListActivity extends AppCompatActivity {

    ListView list;

    //build array to show in list
    String[] ar = new String[]{
            "MainActivity",
            "WebActivity",
            "ImageViewActivity",
            "NextActivity",
            "HoldFragmentsActivity",
            "FragmentHolderActivity",
            "FoodListActivity",
            "GridviewActivity",
            "RecyclerActivity"
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //link xml file
        setContentView(R.layout.activity_list);

        //link ListView widget of activity_list
        //required to set adaptor to pass data in list
        list = (ListView) findViewById(R.id.list);


        //need string adaptor to connect to activity_mylist and pass data in array ar
        //ArrayAdapter<String> adaptor = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, ar);
        ArrayAdapter<String> adaptor = new ArrayAdapter<String>(getApplicationContext(), R.layout.activity_mylist, ar); // ar is data to display

        //to work for onClick event from the list
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //get string from array ar
                String pos = ar[position];
                Log.i("activity_name:",pos);

                //convert to class so that we can init to intent

                try {

                    //give full location to choosen activity
                    Class ourclass = Class.forName("com.example.sanjok.myapplication."+pos);

                    //getApplicationContext gives current activity, this
                    Intent i = new Intent(getApplicationContext(), ourclass);

                    //start life
                    startActivity(i);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

            }
        });

        list.setAdapter(adaptor);

    }
}
