package com.example.sanjok.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class NextActivity extends AppCompatActivity{

    Button send_btn;
    EditText edit_text;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_next);

        edit_text = (EditText) findViewById(R.id.edit_txt);

        send_btn = (Button) findViewById(R.id.send_btn);

        send_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //edit_text.getText() get binary value so convert to string
                String txt = edit_text.getText().toString();

                Intent intent = new Intent(NextActivity.this, ReceiverActivity.class);

                intent.putExtra("next_key", txt);

                startActivity(intent);


            }
        });

        //String value = getIntent().getExtras().getString("value");

    }







}
