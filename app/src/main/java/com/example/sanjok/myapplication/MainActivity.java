package com.example.sanjok.myapplication;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView text;
    EditText send_text;
    int i=0;
    Button btn1, btn2, btn_next, send;

    //AlertDialog.Builder alert = new AlertDialog.Builder(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text = (TextView) findViewById(R.id.text);
        btn1 = (Button) findViewById(R.id.add);
        btn2 = (Button) findViewById(R.id.sub);
        btn_next = (Button) findViewById(R.id.next);
        send = (Button) findViewById(R.id.send);


        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i++;
                text.setText(String.valueOf(i));

            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i--;
                text.setText(String.valueOf(i));

            }
        });


        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this, NextActivity.class);

                startActivity(in);
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                send_text = (EditText) findViewById(R.id.send_text);
                String txt = send_text.getText().toString();
                Log.d("txt",txt);


                Intent intent = new Intent(MainActivity.this, NextActivity.class);
                intent.putExtra("value", txt);
            }
        });



    }
}
