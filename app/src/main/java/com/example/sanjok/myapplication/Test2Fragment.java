package com.example.sanjok.myapplication;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Test2Fragment extends Fragment {
    TextView display;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_test2, container, false);
        display = (TextView) v.findViewById(R.id.my_text_view);

        return v;
    }

    public void display(String data){
        display.setText(data);
    }
}
