package com.example.sanjok.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by sanjok on 9/3/17.
 */

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);


        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //show this activitiy for 2seconds
                    Thread.sleep(2000);

                    //link to another layout
                    Intent intent = new Intent(SplashActivity.this, ListActivity.class);

                    //execute mainlayout layout
                    startActivity(intent);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();

    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
