package com.example.sanjok.myapplication;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;


public class DisplayItemActivity extends AppCompatActivity {

    ImageView image;
    TextView title, desc, price;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_display_item);

        image = (ImageView) findViewById(R.id.image);

        title = (TextView) findViewById(R.id.title);
        desc = (TextView) findViewById(R.id.desc);
        price = (TextView) findViewById(R.id.price);

        int position = getIntent().getExtras().getInt("position");
        String foodName = getIntent().getExtras().getString("food_name");
        String foodPrice = getIntent().getExtras().getString("food_price");
        String foodDesc = getIntent().getExtras().getString("food_desc");


        GridAdaptor ad = new GridAdaptor(getApplicationContext());
        image.setImageResource(ad.imgId[position]);

        title.setText(foodName);
        desc.setText(foodDesc);
        price.setText(foodPrice);
    }

}
