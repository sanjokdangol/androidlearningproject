package com.example.sanjok.myapplication;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by sanjok on 9/6/17.
 */

public class WebActivity extends AppCompatActivity {

    WebView web;
    EditText txt;

    WebViewClient client;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_web);

        //link elements
        web = (WebView) findViewById(R.id.web);
        txt = (EditText) findViewById(R.id.txt);


        //init client to web element
        client = new WebViewClient();
        web.setWebViewClient(client);

        //configuration
        WebSettings webset = web.getSettings();
        webset.setJavaScriptEnabled(true);
        webset.setUseWideViewPort(true);
        webset.getLoadWithOverviewMode();
        webset.setSupportZoom(true);
        web.loadUrl("http://www.google.com");


    }

    //onClick method applied here
    public void go(View view) {
        String website = txt.getText().toString();
        web.loadUrl("http://"+website);

    }

    public void back(View view) {
        if(web.canGoBack()){
            web.goBack();
        }else{
            Toast.makeText(getApplicationContext(), "cannot go back", Toast.LENGTH_SHORT).show();//getApplicationContext() in place of this
        }
    }

    public void forward(View view) {
        if(web.canGoForward()){
            web.goForward();
        }else{
            Toast.makeText(getApplicationContext(), "cannot go forward", Toast.LENGTH_SHORT).show();
        }
    }

    public void reload(View view) {
        web.reload();
    }

    public void clear(View view) {
        web.clearHistory();
    }
}
