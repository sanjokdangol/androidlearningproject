package com.example.sanjok.myapplication;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


public class FoodListActivity extends AppCompatActivity {

    ListView foodList;

    ArrayList<FoodItems> items;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list_layout);

        foodList = (ListView) findViewById(R.id.food_list);

        //return result
        items = setFoodItems();

        FoodAdaptor adaptor = new FoodAdaptor(getApplicationContext(), items);

        foodList.setAdapter(adaptor);



    }

    public ArrayList<FoodItems> setFoodItems(){
        ArrayList<FoodItems> result = new ArrayList<>();

        FoodItems item;

        item = new FoodItems();
        item.setFoodTitle("Burger");
        item.setFoodPrice("Rs.200");
        item.setFoodDesc("Spicy");
        result.add(item);

        item = new FoodItems();
        item.setFoodTitle("Hot dog");
        item.setFoodPrice("Rs.300");
        item.setFoodDesc("Spicy");
        result.add(item);

        item = new FoodItems();
        item.setFoodTitle("Ham Burger");
        item.setFoodPrice("Rs.250");
        item.setFoodDesc("Spicy testy");
        result.add(item);

        item = new FoodItems();
        item.setFoodTitle("Chicken Burger");
        item.setFoodPrice("Rs.200");
        item.setFoodDesc("Fried chicken sweet");
        result.add(item);

        item = new FoodItems();
        item.setFoodTitle("Spring Roll");
        item.setFoodPrice("Rs.400");
        item.setFoodDesc("details of sour");
        result.add(item);

        item = new FoodItems();
        item.setFoodTitle("Buff MoMo");
        item.setFoodPrice("Rs.80");
        item.setFoodDesc("Stream");
        result.add(item);

        item = new FoodItems();
        item.setFoodTitle("Chicken MoMo");
        item.setFoodPrice("Rs.100");
        item.setFoodDesc("stream");
        result.add(item);

        item = new FoodItems();
        item.setFoodTitle("Chaumin");
        item.setFoodPrice("Rs.60");
        item.setFoodDesc("Fried");
        result.add(item);


        return result;
    }



}


class FoodAdaptor extends BaseAdapter{

    private Context context;

    private ArrayList<FoodItems> foodItems;

    private LayoutInflater inflater;

    //holds the memory address in integer
    int imgId[] = {
            R.drawable.bb1,
            R.drawable.bb2,
            R.drawable.bb4,
            R.drawable.bb5,
            R.drawable.bb6,
            R.drawable.d1,
            R.drawable.p1,
            R.drawable.p2
    };


    //create constructor
    public FoodAdaptor(Context c, ArrayList<FoodItems> items) {
        this.context = c;
        this.foodItems = items;

        //attach list to xml view
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return foodItems.size();
    }

    @Override
    public Object getItem(int position) {
        return foodItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //convertView single item view in a list

        ViewHolder holder = new ViewHolder();

        if(convertView == null){
            convertView = inflater.inflate(R.layout.activity_sub_layout, null);

            holder.foodImage = (ImageView) convertView.findViewById(R.id.list_image);
            holder.foodTitle = (TextView) convertView.findViewById(R.id.foodTitle);
            holder.foodPrice = (TextView) convertView.findViewById(R.id.foodPrice);
            holder.foodDesc = (TextView) convertView.findViewById(R.id.desc);

            //give individual key value
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.foodImage.setImageResource(imgId[position]);

        holder.foodTitle.setText(foodItems.get(position).getFoodTitle());
        holder.foodPrice.setText(foodItems.get(position).getFoodPrice());
        holder.foodDesc.setText(foodItems.get(position).getFoodDesc());

        return convertView;
    }

    //inner class
    // to catch view
    //to handle onscroll crash problem
    private static class ViewHolder{

        ImageView foodImage;

        TextView foodTitle, foodPrice, foodDesc;
    }
}
