package com.example.sanjok.myapplication;

/**
 * Created by sanjok on 9/10/17.
 */

public interface FragmentInterface {

    void communicate(String data);
}
