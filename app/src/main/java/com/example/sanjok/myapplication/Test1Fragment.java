package com.example.sanjok.myapplication;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

//sending data from fragment1 to activity
//fragments cannot communicate with other fragments directly so first send data to activity first
public class Test1Fragment extends Fragment {

    EditText edit_txt;
    Button send_btn;
    FragmentInterface communicator;// this interface is created within packages

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        //link to layout
        View v = inflater.inflate(R.layout.fragment_test1, container, false);
        edit_txt = (EditText) v.findViewById(R.id.my_edit);

        send_btn = (Button) v.findViewById(R.id.my_send_btn);

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        communicator = (FragmentInterface) getActivity(); //get the activitiy which implements FragmentInterface

        send_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String data = edit_txt.getText().toString();
                communicator.communicate(data);//data pass to another Activity
            }
        });
    }
}
