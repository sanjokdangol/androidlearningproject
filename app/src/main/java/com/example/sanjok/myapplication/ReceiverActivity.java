package com.example.sanjok.myapplication;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class ReceiverActivity extends AppCompatActivity {

    TextView display_txt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //link activity_receiver
        setContentView(R.layout.activity_receiver);

        display_txt = (TextView) findViewById(R.id.display_txt);

        //get value from intent
        String value = getIntent().getStringExtra("next_key");

        display_txt.setText(value);

//splash
        //implicit

    }

}
