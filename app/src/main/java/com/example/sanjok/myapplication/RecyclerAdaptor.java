package com.example.sanjok.myapplication;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by sanjok on 9/14/17.
 * need to configure before implementing methods of abstract class
 * at first create inner class
 * put <RecyclerAdaptor.ViewHolder>
 *     implement methods
 */

public class RecyclerAdaptor extends RecyclerView.Adapter<RecyclerAdaptor.ViewHolder> {

    private Context c;
    private static ArrayList<FoodItems> foodItems;
    LayoutInflater inflater;

    int imgId[] = {
            R.drawable.bb1,
            R.drawable.bb2,
            R.drawable.bb4,
            R.drawable.bb5,
            R.drawable.bb6,
            R.drawable.d1,
            R.drawable.p1,
            R.drawable.p2
    };

    public RecyclerAdaptor(Context context, ArrayList<FoodItems> items){
        this.c = context;
        this.foodItems = items;

        //set context to inflator
        inflater = LayoutInflater.from(c);



    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //provide view to items
        View v = inflater.inflate(R.layout.activity_sub_layout, parent, false);

        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.foodImage.setImageResource(imgId[position]);
        holder.foodPrice.setText(foodItems.get(position).getFoodPrice());
        holder.foodDesc.setText(foodItems.get(position).getFoodDesc());
        holder.foodTitle.setText(foodItems.get(position).getFoodTitle());


    }

    @Override
    public int getItemCount() {
        return foodItems.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView foodImage;
        TextView foodTitle, foodDesc, foodPrice;
        View layout;


        public ViewHolder(View itemView) {
            super(itemView);

            this.layout = itemView;

            foodImage = (ImageView) layout.findViewById(R.id.list_image);
            foodTitle = (TextView) layout.findViewById(R.id.foodTitle);
            foodDesc = (TextView) layout.findViewById(R.id.desc);
            foodPrice = (TextView) layout.findViewById(R.id.foodPrice);

            //for click event
            layout.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

            //provide activity class where view is attached
            Context context = v.getContext();

            //get position of each view of layout
            int position = getLayoutPosition();

            //send data to display item activity
            Intent i = new Intent(context, DisplayItemActivity.class);
            i.putExtra("position",position);
            i.putExtra("food_name", foodItems.get(position).getFoodTitle());
            i.putExtra("food_price", foodItems.get(position).getFoodPrice());
            i.putExtra("food_desc", foodItems.get(position).getFoodDesc());

            //it is important to set flag to work with intent inside adaptor
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            //we cannot access intent directly in adaptor
            context.startActivity(i);


        }
    }
}
