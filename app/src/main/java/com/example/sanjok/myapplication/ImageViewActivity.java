package com.example.sanjok.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;


public class ImageViewActivity extends AppCompatActivity implements View.OnClickListener{

    Button camara, call, email, url;

    ImageView img;

    Button show;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_imageview);

        camara = (Button) findViewById(R.id.camara);
        call = (Button) findViewById(R.id.call);
        email = (Button) findViewById(R.id.email);
        url = (Button) findViewById(R.id.url);

        img = (ImageView) findViewById(R.id.img);

        show = (Button) findViewById(R.id.show);

        camara.setOnClickListener(this);
        call.setOnClickListener(this);
        email.setOnClickListener(this);
        url.setOnClickListener(this);

        show.setOnClickListener(this);
    }




    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch(id){
            case R.id.camara:

                Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(i, 101); //trigger onActivityResult method below

                break;

            case R.id.call:

                Intent call_intent = new Intent(Intent.ACTION_DIAL); //Action_Call   :call directly
                call_intent.setData(Uri.parse("tel:9841587154")); //URI resource indicator  must use tel: it is universal standard
                startActivity(call_intent);
                break;

            case R.id.email:

                Intent email_intent = new Intent(Intent.ACTION_SEND);
                email_intent.setType("message/rfc822"); //MIME

                //collet data to send
                email_intent.putExtra(Intent.EXTRA_EMAIL,new String[]{"sanjog.dangol@gmail.com","dangol.suroj@gmail.com"});
                email_intent.putExtra(Intent.EXTRA_SUBJECT,"example subject");
                email_intent.putExtra(Intent.EXTRA_TEXT,"example message");

                startActivity(email_intent);
                break;

            case R.id.url:
                Intent url_intent = new Intent(Intent.ACTION_VIEW);
                url_intent.setData(Uri.parse("http://www.google.com"));
                startActivity(url_intent);

                break;

            case R.id.show:
                Intent intent_web = new Intent(ImageViewActivity.this, WebActivity.class);
                startActivity(intent_web);
                break;



        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //after returning from camara capturing image
        if(requestCode == 101){
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");

            if(data == null){
                Toast.makeText(this, "return with no data", Toast.LENGTH_SHORT).show();

            }else{
                //display image to ImageView
                img.setImageBitmap(bitmap);
            }


        }else{
            Toast.makeText(this, "return with no data", Toast.LENGTH_SHORT).show();
        }

    }
}
