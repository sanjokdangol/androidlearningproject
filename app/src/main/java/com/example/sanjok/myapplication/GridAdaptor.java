package com.example.sanjok.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.zip.Inflater;

/**
 * Created by sanjok on 9/13/17.
 */

public class GridAdaptor extends BaseAdapter {

    private Context c;
    private ArrayList<FoodItems> foodItems;
    private LayoutInflater inflater;

    int imgId[] = {
            R.drawable.bb1,
            R.drawable.bb2,
            R.drawable.bb4,
            R.drawable.bb5,
            R.drawable.bb6,
            R.drawable.d1,
            R.drawable.p1,
            R.drawable.p2
    };

    GridAdaptor(Context c){
        this.c = c;
    }

    public GridAdaptor(Context context, ArrayList<FoodItems> items){
        this.c = context;
        this.foodItems = items;

        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return foodItems.size();
    }

    @Override
    public Object getItem(int position) {
        return foodItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //create object of ViewHolder
        ViewHolder holder = new ViewHolder();

        if(convertView == null){
            convertView = inflater.inflate(R.layout.activity_sub_gridview, null);

            holder.foodTitle = (TextView) convertView.findViewById(R.id.foodTitle);
            holder.foodDesc = (TextView) convertView.findViewById(R.id.foodDesc);
            holder.foodPrice = (TextView) convertView.findViewById(R.id.foodPrice);
            holder.foodImage = (ImageView) convertView.findViewById(R.id.foodImage);


            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.foodImage.setImageResource(imgId[position]);
        holder.foodTitle.setText(foodItems.get(position).getFoodTitle());
        holder.foodPrice.setText(foodItems.get(position).getFoodPrice());
        holder.foodDesc.setText(foodItems.get(position).getFoodDesc());

        return convertView;
    }


    public static class ViewHolder{
        ImageView foodImage;
        TextView foodTitle, foodDesc, foodPrice;
    }
}
